package be.intectbrussel.circle;

public class Circle {
	
	private int diameter;
	private int radius;
	private int x;
	private int y;
	
	public static int ANGLES;
	private static int count;
	
	static {
		ANGLES = 4;
	}
	{
		// setCount(getCount() + 1);
		count++;
	}
	
	public Circle() {
		this(0, 0, 0, 0);
		
	}
	
	public Circle(int radius, int diameter, int x, int y) {
		setRadius(radius);
		setX(x);
		setY(y);
	}
	
	public void setX(int x) {
		this.x = x;
	}
	
	public void setY(int y) {
		this.y = y;
	}
	
	public int getX() {
		return x;
	}
	
	public int getY() {
		return y;
	}
	
	public void setGrow(int d) {
		setRadius(radius * d);	
	}
	
	public void setPosition(int x, int y) {
		setX(x);
		setY(y);
	}
	
	public void setRadius(int radius) {
		if (radius < 0) {
			this.radius = radius * (-1);
		}else if (radius == 0){
			this.radius = 0;
		}else {
			this.radius = radius;
		}
	}
	
	public int getRadius() {
		return radius;
	}
	
	public void setDiameter(int diameter) {
		if (diameter < 0) {
			this.diameter = diameter * (-1);
		}else if (diameter == 0){
			this.diameter = 0;
		}else {
			this.diameter = diameter;
		}
	}
	
	public int getDiameter() {
		return diameter;
	}
	
	
	public double getArea() {
		return Math.PI*(radius*radius);
	}
	
	public double getPerimeter() {
		return 2*Math.PI*radius;
	}
	
	public static int getCount() {
		return count;
	}
	

}
