package be.intectbrussel.circle;

public class CircleApp {
	public static void main(String[] args) {

		System.out.println("-----------------------------------");
		System.out.println("This program uses a circle");
		System.out.println("-----------------------------------");

		Circle circ = new Circle(14, 50, 30, 65);
		Circle circ2 = new Circle(45, 91, 50, 48);

		circ2.setPosition(45, 50);
		circ2.setRadius(48);

		circ.setGrow(2);
		
		System.out.println("First circle area: " + circ.getArea());
		System.out.println("First circle diameter: " + circ.getDiameter());
		System.out.println("First circle perimeter: " + circ.getPerimeter());
		System.out.println("First circle radius: " + circ.getRadius());
		
		System.out.println("-----------------------------------");
		System.out.println("Second Rectangle");
		System.out.println("-----------------------------------");

		System.out.println("Second circle area: " + circ2.getArea());
		System.out.println("Second circle diameter: " + circ2.getDiameter());
		System.out.println("Second circle perimeter: " + circ2.getPerimeter());
		System.out.println("Second circle radius: " + circ2.getRadius());
		
		System.out.println("Count = " + Circle.getCount());

	}

}
