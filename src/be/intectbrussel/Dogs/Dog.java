package be.intectbrussel.Dogs;

import java.util.*;

public class Dog {

	private static String bark() {
		return "Kef kef!";
	}

	private static String beg() {
		return "C'mon please!";
	}

	private static String chase() {
		return "I'm gonna get you!";
	}

	public static void react(String command) {

		if (command.equals("speak")) {
			System.out.println(bark());
		} else if (command.equals("beg")) {
			System.out.println(beg());
		} else if (command.equals("look, the postman!")) {
			System.out.println(chase());
		} else {
			System.out.println("Please try again.");
		}

	}

	public static void main(String[] args) {

		System.out.println("Enter your command: [speak], [beg], [look, the postman!] ");

		Scanner keyboard = new Scanner(System.in);
		String command = keyboard.nextLine();
		react(command);

		keyboard.close();
	}

}
