package be.intecbrussel.zoo;

public class Tiger {
	
	private String name;
	private int weight;
	private char sex;
	private int age;
	
	private static Tiger[] collection;
	
	public Tiger(String name, int weight, char sex, int age) {
		setName(name);
		setWeight(weight);
		setSex(sex);
		setAge(age);
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	
	public void setWeight(int weight) {
		this.weight = weight;
	}
	
	public int getWeight() {
		return weight;
	}
	
	public void setSex(char sex) {
		this.sex = sex;
	}
	
	public char getSex() {
		return sex;
	}
	
	public void setAge(int age) {
		this.age = age;
	}
	
	public int getAge() {
		return age;
	}
	
	public static void setCollectionTiger(Tiger[] tigers) {
		collection = tigers;
	}

	public static Tiger[] getCollectionTiger() {
		return collection;
	}

	@Override
	public String toString() {
		return "Tiger [name=" + name + ", weight=" + weight + ", sex=" + sex + ", age=" + age + "]";
	}

}
