package be.intecbrussel.zoo;

public class Lion {
	
	private String name;
	private int weight;
	private char sex;
	private int age;
	
	private static Lion[] collection;
	
	public Lion(String name, int weight, char sex, int age) {
		setName(name);
		setWeight(weight);
		setSex(sex);
		setAge(age);
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	
	public void setWeight(int weight) {
		this.weight = weight;
	}
	
	public int getWeight() {
		return weight;
	}
	
	public void setSex(char sex) {
		this.sex = sex;
	}
	
	public char getSex() {
		return sex;
	}
	
	public void setAge(int age) {
		this.age = age;
	}
	
	public int getAge() {
		return age;
	}
	
	public static void setCollectionLion(Lion[] lions) {
		collection = lions;
	}

	public static Lion[] getCollectionLion() {
		return collection;
	}

	@Override
	public String toString() {
		return "Lion [name=" + name + ", weight=" + weight + ", sex=" + sex + ", age=" + age + "]";
	}

}
