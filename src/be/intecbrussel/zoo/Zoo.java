package be.intecbrussel.zoo;

public class Zoo {
	public static void main(String[] args) {

		Monkey[] monkeys = new Monkey[5];
		Hippo[] hippos = new Hippo[5];
		Lion[] lions = new Lion[5];
		Tiger[] tigers = new Tiger[5];
		Flamingo[] flamingos = new Flamingo[5];

		// MONKEYS
		monkeys[0] = new Monkey("Mateo", 15, 'm', 22);
		monkeys[1] = new Monkey("Maria", 20, 'v', 20);
		monkeys[2] = new Monkey("Flip", 15, 'm', 18);
		monkeys[3] = new Monkey("Felice", 50, 'm', 45);
		monkeys[4] = new Monkey("Helena", 9, 'v', 3);
		Monkey.setCollectionMonkey(monkeys);
		monkeys = Monkey.getCollectionMonkey();

		for (Monkey m : monkeys) {
			System.out.println(m);
		}

		// HIPPOS
		hippos[0] = new Hippo("Hip", 200, 'm', 30);
		hippos[1] = new Hippo("Hop", 191, 'v', 44);
		hippos[2] = new Hippo("Pot", 91, 'm', 55);
		hippos[3] = new Hippo("Fred", 154, 'm', 20);
		hippos[4] = new Hippo("Lucy", 233, 'v', 30);
		Hippo.setCollectionHippo(hippos);
		hippos = Hippo.getCollectionHippo();

		for (Hippo h : hippos) {
			System.out.println(h);
		}

		// LIONS
		lions[0] = new Lion("Mufassa", 80, 'm', 30);
		lions[1] = new Lion("Lana", 70, 'v', 20);
		lions[2] = new Lion("Scar", 50, 'm', 20);
		lions[3] = new Lion("Kofu", 66, 'm', 10);
		lions[4] = new Lion("Snoop", 20, 'm', 50);
		Lion.setCollectionLion(lions);
		lions = Lion.getCollectionLion();

		for (Lion l : lions) {
			System.out.println(l);
		}

		// TIGERZ
		tigers[0] = new Tiger("George", 90, 'm', 54);
		tigers[1] = new Tiger("Marissa", 80, 'v', 23);
		tigers[2] = new Tiger("Titi", 70, 'm', 45);
		tigers[3] = new Tiger("Joris", 60, 'm', 21);
		tigers[4] = new Tiger("Hedwig", 50, 'v', 33);
		Tiger.setCollectionTiger(tigers);
		tigers = Tiger.getCollectionTiger();

		for (Tiger t : tigers) {
			System.out.println(t);
		}

		// FLAMINGO
		flamingos[0] = new Flamingo("Fla", 45, 'm', 22);
		flamingos[1] = new Flamingo("Ming", 55, 'v', 20);
		flamingos[2] = new Flamingo("Go", 20, 'm', 18);
		flamingos[3] = new Flamingo("Scam", 23, 'm', 45);
		flamingos[4] = new Flamingo("Pi", 29, 'v', 3);
		Flamingo.setCollectionFlamingo(flamingos);
		flamingos = Flamingo.getCollectionFlamingo();

		for (Flamingo f : flamingos) {
			System.out.println(f);
		}

	}

}
