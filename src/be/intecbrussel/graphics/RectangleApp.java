package be.intecbrussel.graphics;

import static be.intecbrussel.graphics.Rectangle.*;

public class RectangleApp {
	public static void main(String[] args) {

		System.out.println("-----------------------------------");
		System.out.println("This program uses a rectangle");
		System.out.println("-----------------------------------");

		// RECTANGLES
		Rectangle rect = new Rectangle(22, 14, 12, 63);
		Rectangle copy = new Rectangle(38, 44, 14, 47);
		Rectangle r = new Rectangle(44, 50, 63, 75);
		Rectangle y = new Rectangle(19, 06, 19, 91);

		System.out.println("First rectangle height: " + rect.getHeight());
		System.out.println("First rectangle width: " + rect.getWidth());
		System.out.println("First rectangle x: " + rect.getX());
		System.out.println("First rectangle y: " + rect.getY());

		System.out.println("Area of rectangle: " + rect.getArea());
		System.out.println("Perimeter of rectangle: " + rect.getPerimeter());

		System.out.println("-----------------------------------");
		System.out.println("Grow Second Rectangle");
		System.out.println("-----------------------------------");

		// MAKE RECTANGLE GROW
		copy.grow(2);

		System.out.println("Second rectangle height: " + copy.getHeight());
		System.out.println("Second rectangle width: " + copy.getWidth());
		System.out.println("Second rectangle x: " + copy.getX());
		System.out.println("Second rectangle y: " + copy.getY());

		System.out.println("Area of second rectangle: " + copy.getArea());
		System.out.println("Perimeter of second rectangle: " + copy.getPerimeter());

		System.out.println("-----------------------------------");
		System.out.println("Turd Rectangle");
		System.out.println("-----------------------------------");

		System.out.println("Third rectangle height: " + r.getHeight());
		System.out.println("Third rectangle width: " + r.getWidth());
		System.out.println("Third rectangle x: " + r.getX());
		System.out.println("Third rectangle y: " + r.getY());
		
		System.out.println("Area of third rectangle: " + r.getArea());
		System.out.println("Perimeter of third rectangle: " + r.getPerimeter());

		System.out.println("-----------------------------------");
		System.out.println("Angles and count");
		System.out.println("-----------------------------------");
		System.out.println("Angles = " + Rectangle.ANGLES);
		// DOOR PAKKET STATIC IMPORT BOVENAAN KAN JE DE GETCOUNT GEBRUIKEN IPV
		// RECTANGLE.COUNT
		System.out.println("Count = " + getCount());

	}

}
