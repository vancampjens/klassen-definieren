package be.intecbrussel.graphics;

public class Rectangle {

	private int x;
	private int y;
	private int height;
	private int width;

	public static int ANGLES;
	private static int count;

	static {
		ANGLES = 4;
	}
	{
		// setCount(getCount() + 1);
		count++;
	}

	public Rectangle() {
		this(0, 0, 0, 0);
	}

	public Rectangle(int height, int width) {
		this(height, width, 0, 0);
	}

	// CONSTRUCTOR
	public Rectangle(int height, int width, int x, int y) {
		setHeight(height);
		setWidth(width);
		setX(x);
		setY(y);
	}

	public Rectangle(Rectangle rect) {
		// setHeight(rect.getHeight());
		// setWidth(rect.getWidth());
		// setPosition(rect.getX(), rect.getY());

		this(rect.getHeight(), rect.getWidth(), rect.getX(), rect.getY());
	}

	public void setHeight(int height) {
		// LAAT WAARDE ONDER 0 NIET TOE
		this.height = height < 0 ? -height : height;
	}

	public int getHeight() {
		return height;
	}

	public void setWidth(int width) {
		this.width = width < 0 ? -width : width;
	}

	public int getWidth() {
		return width;
	}

	public void setPosition(int x, int y) {
		setX(x);
		setY(y);
	}

	public void setX(int x) {
		if (x < 0) {
			this.x = x * -1;
		} else if (x == 0) {
			this.x = 1;
		} else {
			this.x = x;
		}
	}

	public int getX() {
		return x;
	}

	public void setY(int y) {
		if (y < 0) {
			this.y = y * -1;
		} else if (y == 0) {
			this.y = 1;
		} else {
			this.y = y;
		}

	}

	public int getY() {
		return y;
	}

	public void grow(int d) {
		setHeight(getHeight() * d);
		setWidth(getWidth() * d);

		this.height = (this.height * d) < 0 ? this.height * -1 : this.height;
		this.width = (this.width * d) < 0 ? this.width * -1 : this.width;
	}

	public double getArea() {
		return (height * width);
	}

	public double getPerimeter() {
		double perimeter = (height * 2) + (width * 2);
		return perimeter;
	}
	
	public static int getCount() {
		return count;
	}

}
