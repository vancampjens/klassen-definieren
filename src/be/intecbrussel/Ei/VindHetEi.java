package be.intecbrussel.Ei;

import java.util.Random;
import java.util.Scanner;

public class VindHetEi {

	public static void main(String[] args) {

		// PRINT KOLOM
		char[][] eiKolom = new char[10][10];
		int i, j = 'X';

		for (i = 0; i < eiKolom.length; i++) {
			for (j = 0; j < eiKolom[i].length; j++) {
				eiKolom[i][j] = 'X';

				System.out.print(eiKolom[i][j] + " ");

			}

			System.out.println();

		}

		// GENERATE RANDOM

		Random rand = new Random();
		int xPos = rand.nextInt(10) + 1;
		int yPos = rand.nextInt(10) + 1;

		Scanner keyboard = new Scanner(System.in);

		// TIJDELIJKE PRINT
		//System.out.println(xPos);
		//System.out.println(yPos);

		// PROGRAM
		System.out.println("Voeg een x-coordinaat in tussen 1 en 10");
		int xInput = keyboard.nextInt();
		System.out.println("Voeg een y-coordinaat in tussen 1 en 10");
		int yInput = keyboard.nextInt();

		do {
			if ((xInput == xPos) && (yInput == yPos)) {

				eiKolom[yPos - 1][xPos - 1] = '0';
				for (char[] row : eiKolom) {
					for (char el : row) {
						System.out.print(el + " ");
					}
					System.out.println();
				}
				System.out.println("Ei gevonden!");
				break;
			}

			eiKolom[yInput - 1][xInput - 1] = '_';
			for (char[] row : eiKolom) {
				for (char el : row) {
					System.out.print(el + " ");
				}
				System.out.println();
			}
			
			System.out.println("Probeer opnieuw!");
			System.out.println("Voeg een x-coordinaat in tussen 1 en 10");
			xInput = keyboard.nextInt();
			System.out.println("Voeg een y-coordinaat in tussen 1 en 10");
			yInput = keyboard.nextInt();

		} while ((xInput != xPos) && (yInput != yPos));

		eiKolom[yPos - 1][xPos - 1] = '0';
		for (char[] row : eiKolom) {
			for (char el : row) {
				System.out.print(el + " ");
			}
			System.out.println();
		}
		System.out.println("Ei gevonden!");

		keyboard.close();
	}

}
