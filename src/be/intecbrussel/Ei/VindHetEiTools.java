package be.intecbrussel.Ei;

public class VindHetEiTools {

	public static void printTips(int yPos, int yInput, int xPos, int xInput) {
		if (yPos > yInput && xPos > xInput) {
			System.out.println("Wrong guess, try a little bit more down and to the right. Again!");
		} else if (yPos > yInput && xPos < xInput) {
			System.out.println("Wrong guess, try a little bit more down and to the left. Again!");
		} else if (yPos < yInput && xPos < xInput) {
			System.out.println("Wrong guess, try a little bit more up and to the left. Again!");
		} else if (yPos < yInput && xPos > xInput) {
			System.out.println("Wrong guess, try a little bit more up and to the right. Again!");
		} else if (yPos > yInput && xPos == xInput) {
			System.out.println("Wrong guess, try a little bit more down. Again!");
		} else if (yPos < yInput && xPos == xInput) {
			System.out.println("Wrong guess, try a little bit more up. Again!");
		} else if (yPos == yInput && xPos > xInput) {
			System.out.println("Wrong guess, try a little bit more to the right. Again!");
		} else if (yPos == yInput && xPos < xInput) {
			System.out.println("Wrong guess, try a little bit more to the left. Again!");
		}

	}

}
